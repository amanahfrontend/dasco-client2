import { Component, OnInit, DoCheck, Inject } from '@angular/core';
import { AuthenticationServicesService } from "./api-module/services/authentication/authentication-services.service";
import { UtilitiesService } from "./api-module/services/utilities/utilities.service";
import { DOCUMENT } from '@angular/common';
import { LookupService } from './api-module/services/lookup-services/lookup.service';

interface Item {
  title: string,
  path: string,
  icon: string
  roles: Array<String>
}

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})



export class AppComponent implements OnInit, DoCheck {
  title = 'app';
  menuItemToggle: boolean;
  roles: string[];
  toggleFullWidth: any;
  template: string = `<img src="http://pa1.narvii.com/5722/2c617cd9674417d272084884b61e4bb7dd5f0b15_hq.gif" />`

  items: Item[] = [
    {
      title: 'Admin page',
      path: './admin',
      icon: 'fa-cogs',
      roles: [
        'Admin'
      ]
    },
    {
      title: 'Start Call',
      path: './search',
      icon: 'fa-user-plus',
      roles: [
        'CallCenter'
      ]
    },
    {
      title: 'Calls history',
      path: './search/calls-history',
      icon: 'fa-phone',
      roles: [
        'CallCenter', 'Maintenance'
      ]
    },
    {
      title: 'Customers',
      path: './search/customersList',
      icon: 'fa-users',
      roles: [
        'CallCenter', 'Maintenance'
      ]
    },
    {
      title: 'Estimations',
      path: './search/estimation',
      icon: 'fa-file-text',
      roles: [
        'Maintenance'
      ]
    },
    {
      title: 'Quotations',
      path: './search/quotation',
      icon: 'fa-usd',
      roles: [
        'Maintenance', 'CallCenter'
      ]
    },
    {
      title: 'Contracts',
      path: './search/contract',
      icon: 'fa-book',
      roles: [
        'CallCenter'
      ]
    },
    {
      title: 'Orders',
      path: './search/order',
      icon: 'fa-briefcase',
      roles: [
        'CallCenter'
      ]
    },
    {
      title: 'Orders Management',
      path: './dispatcher',
      icon: 'fa-briefcase',
      roles: [
        'SupervisorDispatcher', 'Dispatcher'
      ]
    },
    {
      title: 'Preventive Orders',
      path: './dispatcher/preventive',
      icon: 'fa-history',
      roles: [
        'SupervisorDispatcher', 'Dispatcher'
      ]
    },
    // {
    //   title: 'Estimations Report',
    //   path: './search/estimations-report',
    //   icon:'fa-briefcase',
    //   roles: [
    //   'Maintenance','CallCenter'
    //   ]
    // },
    {
      title: 'Report',
      path: './dispatcher/report',
      icon: 'fa-table',
      roles: [
        'SupervisorDispatcher', 'Dispatcher', 'Admin'
      ]
    },
    {
      title: 'Visit Time Calender',
      path: './dispatcher/calender',
      icon: 'fa-calendar',
      roles: [
        'SupervisorDispatcher', 'Dispatcher'
      ]
    },
    {
      title: 'Inventory',
      path: './assign-item',
      icon: 'fa-industry',
      roles: [
        'SupervisorDispatcher', 'Dispatcher', 'Admin'
      ]
    },
  ];

  constructor(
    private authService: AuthenticationServicesService,
    private utilities: UtilitiesService,
    @Inject(DOCUMENT) private document: HTMLDocument,
    private lookupService: LookupService
  ) {

    this.lookupService.getWebsiteSettingsBykey().subscribe(settings => {
      let array = [];
      array = settings;
      array.map(item => {
        if (item.key === 'image') {
          this.document.getElementById('appFavicon').setAttribute('href', item.value);
        }
      })
    });

  }

  ngDoCheck() {
    this.menuItemToggle = this.authService.CurrentUser();
    this.toggleFullWidth = this.utilities.toggleFullWidth;
  }

  ngOnInit() {
    this.authService.activateSignalR();
    this.roles = [];
    this.authService.userRoles.subscribe((roles) => {
      this.roles = roles || [];
    });
  }

  openNav() {
    this.document.getElementById("mySidenav").style.width = "250px";
    this.document.getElementById("main").style.marginRight = "250px";
  }

  closeNav() {
    this.document.getElementById("mySidenav").style.width = "0";
    this.document.getElementById("main").style.marginRight = "0";
  }

  /**
   * 
   * @param source 
   * @param target 
   */
  containsAny(target) {
    let result = this.roles.filter((item) => {
      return target.indexOf(item) > -1;
    });
    return (result.length > 0);
  }

}