import { NgModule, ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuardGuard } from './api-module/guards/auth-guard.guard';

const routes: Routes = [
  {
    path: '',
    loadChildren: './login-register-module/login-register-module.module#LoginRegisterModuleModule'
  },
  {
    path: 'search',
    loadChildren: './customer/customer.module#CustomerModule',
    canActivate: [AuthGuardGuard]
  },
  {
    path: 'admin',
    loadChildren: './admin/admin.module#AdminModule',
    canActivate: [AuthGuardGuard]
  }
  ,
  {
    path: 'dispatcher', loadChildren: './dispatcher/dispatcher.module#DispatcherModule',
    canActivate: [AuthGuardGuard]
  },
  {
    path: 'assign-item', loadChildren: './assign-items/assign-items.module#AssignItemsModule',
    canActivate: [AuthGuardGuard]
  },

  // {
  //   path: 'dashboard',
  //   loadChildren: './dashboard/dashboard.module#DashboardModule',
  //   canActivate: [AuthGuardGuard]
  // },
  // {
  //   path: '',
  //   loadChildren: './login-register-module/login-register-module.module#LoginRegisterModuleModule'
  // },
  {
    path: '**',
    redirectTo: ''
  }


];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class RoutingModule {
}
export const RoutingComponents = [];

export const Routing: ModuleWithProviders = RouterModule.forRoot(routes);
