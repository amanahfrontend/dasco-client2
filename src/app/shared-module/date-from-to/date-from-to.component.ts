import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-date-from-to',
  templateUrl: './date-from-to.component.html',
  styleUrls: ['./date-from-to.component.css']
})

export class DateFromToComponent implements OnInit {

  @Input() submitted: boolean;
  @Input() required: boolean;
  @Input() layout: string;
  @Input() startDateLable: string;
  @Input() endDateLable: string;


  @Output() startDate = new EventEmitter();
  @Output() endDate = new EventEmitter();

  condition: boolean = true;
  date: any = {};
  layoutClass: string;


  constructor() {
  }

  ngOnInit() {
    this.layoutClass = this.layout;
  }

  emitStartDate() {
    this.startDate.emit(this.date.startDate);

  }


  resetStartDate() {
    this.date.startDate = null;
    this.condition = false;
    this.condition = true;

  }


  resetEndDate() {
    this.date.endDate = null;
    this.condition = false;
    this.condition = true;
  }


  emitEndDate() {
    this.endDate.emit(this.date.endDate);
  }
}
