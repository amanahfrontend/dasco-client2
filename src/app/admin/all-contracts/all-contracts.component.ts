import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { LookupService } from "../../api-module/services/lookup-services/lookup.service";
import { Subscription } from "rxjs";
import { Message } from "primeng/components/common/message";
import { UtilitiesService } from "../../api-module/services/utilities/utilities.service";
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";
import { SearchComponent } from "../../shared-module/search/search.component";
import { Page } from '../../shared-module/shared/model/page';

@Component({
  selector: 'app-all-contracts',
  templateUrl: './all-contracts.component.html',
  styleUrls: ['./all-contracts.component.css'],
})

export class AllContractsComponent implements OnInit, OnDestroy {
  allContracts: any[];
  allContractSubscription: Subscription;
  // deleteContractSubscription: Subscription;
  buttonsList: any[];
  page = new Page();
  rows = new Array<any>();
  // activeRow: any;
  bigMessage: Message[] = [];
  contractMessage: Message[] = [];
  cornerMessage: Message[] = [];
  toggleLoading: boolean;

  @ViewChild(SearchComponent)

  searchComponent: SearchComponent;
  allFilteredContracts: any[] = [];
  searchByValue:any;
  
  constructor(private lookup: LookupService, private utilities: UtilitiesService, private modalService: NgbModal) {
    this.page.pageNumber = 1;
    this.page.pageSize = 10;
  }

 

  ngOnInit() {
    this.allContracts = [];
    if (this.utilities.currentSearch != undefined) {
      if (this.utilities.routingFromAndHaveSearch && this.utilities.currentSearch.searchText && this.utilities.currentSearch.searchType == 'contract') {
        this.searchComponent.searchText = this.utilities.currentSearch.searchText;
        this.searchContract(this.utilities.currentSearch.searchText);
      }
      else {
        this.getAllContracts({ offset: 0 });
      }
    }
    else {
      this.getAllContracts({ offset: 0 });
    }
  }

  ngOnDestroy() {
    this.allContractSubscription && this.allContractSubscription.unsubscribe();
  }

  printContracts(): void {
    var newWin = window.open('', '_blank', 'top=0,left=0,height=100%,width=95%');

    var divToPrint = document.getElementById("contract-table");
    // let newWin = window.open("");
    newWin.document.write(divToPrint.outerHTML);
    newWin.print();
    newWin.close();
  }

  getAllContracts(pageNumber) {
    this.toggleLoading = true;
    if (pageNumber > 0)
      this.page.pageNumber = pageNumber;
    else
      this.page.pageNumber = 0;


    let objToPost = Object.assign({}, this.page);
    objToPost.pageNumber = this.page.pageNumber + 1;



    this.allContractSubscription = this.lookup.getAllContractsByPaging(objToPost).subscribe((allContracts) => {
      this.toggleLoading = false;
      this.rows = allContracts.result;
      this.page.totalElements = allContracts.totalCount;
      this.allContracts = allContracts.result;
      let currentDate = new Date;
      let nextDate = new Date(new Date().setMonth(new Date().getMonth() + 1));
      this.allContracts.forEach(element => {
        let date = new Date(`${element.endDate}`);
        if ((date.getTime() > currentDate.getTime() && date.getTime() < nextDate.getTime())) {
          this.allFilteredContracts.push(element);
        }
      });
      // this.allFilteredDates=this.allContracts.filter(x=>this.utilities.convertDatetoNormal(x.endDate)>currenNormaltDate && this.utilities.convertDatetoNormal(x.endDate) < nextDate);
      this.bigMessage = [];
      this.bigMessage.push({
        severity: 'info',
        summary: 'Dropdown arrow',
        detail: 'You can remove any Contract by click the dropdown arrow and choose remove.'
      });
      this.contractMessage = [];
      this.contractMessage.push({
        severity: 'Error',
        summary: 'Dropdown arrow',
        detail: '<button>Click</button>'
      });
    },
      err => {
        this.toggleLoading = false;
        err.status == 401 && this.utilities.unauthrizedAction();
        this.cornerMessage = [];
        this.cornerMessage.push({
          severity: 'error',
          summary: 'Failed',
          detail: 'Failed to load Contracts due to server error!'
        });
      });
  }



  searchContract(searchText) {
    this.toggleLoading = true;
    this.utilities.paginationFlagContract = true;
    let pageNumber;

    if (searchText.searchedParmaters == null || searchText.searchedParmaters == undefined)
      this.utilities.searchedValueContract = searchText;
    else {
      pageNumber = searchText.pageNumber;
      searchText = this.utilities.searchedValueContract;
    }
    if (pageNumber > 0)
      this.page.pageNumber = pageNumber;
    else
      this.page.pageNumber = 0;

    let objToPost = Object.assign({}, this.page);
    objToPost.pageNumber = this.page.pageNumber + 1;
    objToPost.searchBy = searchText;


    this.utilities.currentSearch.searchType = 'contract';
    this.lookup.searchContractBypaging(objToPost).subscribe((contracts) => {
      this.allContracts = contracts.result;
      this.page.totalElements = contracts.totalCount;


      this.allContracts = contracts;
      this.toggleLoading = false;
    },
      err => {
        this.cornerMessage.push({
          severity: "error",
          summary: "Failed",
          detail: "Failed to get data due to network error, please try again later."
        });
        this.toggleLoading = false;
      })
  }



}
