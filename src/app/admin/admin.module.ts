import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { MultiSelectModule } from "primeng/components/multiselect/multiselect";
import { SharedModuleModule } from "../shared-module/shared-module.module";
import { AdminMainPageComponent } from "./admin-main-page/admin-main-page.component";
import { AdminUploadResourcesComponent } from "./admin-upload-resources/admin-upload-resources.component";
import { AdminUsersComponent } from "./admin-users/admin-users.component";
import { AssignTecComponent } from "./assign-tec/assign-tec.component";
import { CustomerListComponent } from './customer-list/customer-list.component';
import { UserService } from "./services/user.service";
import { SettingsComponent } from './settings/settings.component';
import { ReactiveFormsModule } from '@angular/forms';
import { AllContractsComponent } from './all-contracts/all-contracts.component';
import { ContractTableComponent } from './all-contracts/contract-table/contract-table.component';
import { FieldErrorDisplayModule } from './../components/field-error-display/field-error-display.module';
import { SettingDispatchersComponent } from "./setting-dispatchers/setting-dispatchers.component";
import { SimpleAdminContentComponent } from "./simple-admin-content/simple-admin-content.component";
import { EditDisAndProbModalComponent } from "./setting-dispatchers/edit-dis-and-prob-modal/edit-dis-and-prob-modal.component";
import { AdminChangePasswordComponent } from "./admin-users/admin-change-password/admin-change-password.component";
import { RoleGuard } from './../api-module/guards/role.guard';
import { FilterPipe } from './../pipes/filter.pipe';

const routes: Routes = [
  {
    path: "",
    component: AdminMainPageComponent,
    canActivate: [RoleGuard],
    data: { roles: ["Admin"] }
  }
];

@NgModule({
  imports: [
    ReactiveFormsModule,
    CommonModule,
    SharedModuleModule,
    MultiSelectModule,
    FieldErrorDisplayModule,
    RouterModule.forChild(routes),
  ],
  declarations: [
    CustomerListComponent,
    AdminMainPageComponent,
    SimpleAdminContentComponent,
    AdminUsersComponent,
    AdminUploadResourcesComponent,
    AssignTecComponent,
    SettingDispatchersComponent,
    EditDisAndProbModalComponent,
    AdminChangePasswordComponent,
    SettingsComponent,
    AllContractsComponent,
    ContractTableComponent,
    FilterPipe
  ],
  entryComponents: [
    EditDisAndProbModalComponent,
    AdminChangePasswordComponent
  ],
  providers: [UserService, RoleGuard]
})
export class AdminModule {
}
