import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SimpleAdminContentComponent } from './simple-admin-content.component';

describe('SimpleAdminContentComponent', () => {
  let component: SimpleAdminContentComponent;
  let fixture: ComponentFixture<SimpleAdminContentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SimpleAdminContentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SimpleAdminContentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
