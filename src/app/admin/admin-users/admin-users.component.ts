import { Component, Input, OnChanges, OnDestroy } from "@angular/core";
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";
import { MessageService } from "primeng/components/common/messageservice";
import { Subscription } from "rxjs";
import { LookupService } from "../../api-module/services/lookup-services/lookup.service";
import { AddEditUserModalComponent } from "./add-edit-user-modal/add-edit-user-modal.component";
import { AdminChangePasswordComponent } from "./admin-change-password/admin-change-password.component";
import { Angular2Csv } from "angular2-csv";
import Swal from 'sweetalert2';
import { FilterPipe } from './../../pipes/filter.pipe';

@Component({
  selector: "app-admin-users",
  templateUrl: "./admin-users.component.html",
  styleUrls: ["./admin-users.component.css"],
  providers: [
    FilterPipe
  ]
})

export class AdminUsersComponent implements OnDestroy, OnChanges {
  @Input() getData: boolean;

  users: any[];
  tempUsers: any[];

  UsersSubscription: Subscription;
  modalRef: any;
  toggleLoading;
  display: boolean = false;
  public searchString: string;

  constructor(private lookup: LookupService, private messageService: MessageService, private modalService: NgbModal, private filterPipe: FilterPipe) {
  }

  // ngOnInit() {
  //   this.toggleLoading = true;
  // }



  showDialog() {
    this.display = true;
  }

  ngOnChanges() {
    this.toggleLoading = true;

    if (this.getData) {
      this.UsersSubscription = this.lookup.getUsers().subscribe(
        (users) => {
          this.toggleLoading = false;
          this.users = users;
          this.users.map((user) => {
            user.name = `${user.firstName} ${user.middleName} ${user.lastName}`;
          });

          this.tempUsers = this.users;
        },
        err => {
          this.toggleLoading = false;

          this.messageService.add({
            severity: "error",
            summary: "Failed",
            detail: `Failed to load data due to server error.`
          });
        });
    }
  }

  ngOnDestroy() {
    this.UsersSubscription && this.UsersSubscription.unsubscribe();
  }

  edit(user) {
    let userCopy = Object.assign({}, user);

    this.openAddEditUserModal(userCopy, `Edit ${userCopy.name} info`);

    this.modalRef.result.then(
      (editedData) => {
        let userRoles = [];
        userRoles.push(editedData.roleNames);
        editedData.roleNames = userRoles;

        delete editedData.name;
        this.lookup.updateUser(editedData).subscribe(
          () => {

            user = editedData;

            console.log("added User ", user);

            if (editedData.roleNames[0] == 'Technican') {


              let tecWithDis = {
                fK_technican_id: editedData.id,
                fK_dispatcher_id: editedData.selectedDispatureid
              };

              this.lookup.postTecnichanWithDis(tecWithDis).subscribe(() => {

                //this.cornerMessage = [];
                //this.cornerMessage.push({
                //  severity: 'success',
                //  summary: 'successfully',
                //  detail: `Technician ${this.tecToPost.firstName} ${this.tecToPost.middleName} Assigned to dispatcher ${this.disToPost.firstName} ${this.disToPost.middleName} successfully!`
                //});
                //this.tecLoading = false;
              },
                err => {
                  //this.cornerMessage = [];
                  //this.cornerMessage.push({
                  //  severity: 'error',
                  //  summary: 'Failed',
                  //  detail: 'Failed to Assign Technician!'
                  //});
                  //this.tecLoading = false;
                })

            }
            else {
              this.messageService.add({
                severity: "success",
                summary: "Successful!",
                detail: `${user.name} info Edited & Saved Successfully!`
              });

            }




            //let tecWithDis = {
            //  fK_technican_id: this.tecToPost.id,
            //  fK_dispatcher_id: this.disToPost.id
            //};

            //this.lookup.postTecnichanWithDis(tecWithDis).subscribe(() => {
            //  //this.cornerMessage = [];
            //  //this.cornerMessage.push({
            //  //  severity: 'success',
            //  //  summary: 'successfully',
            //  //  detail: `Technician ${this.tecToPost.firstName} ${this.tecToPost.middleName} Assigned to dispatcher ${this.disToPost.firstName} ${this.disToPost.middleName} successfully!`
            //  //});
            //  //this.tecLoading = false;
            //},
            //  err => {
            //    //this.cornerMessage = [];
            //    //this.cornerMessage.push({
            //    //  severity: 'error',
            //    //  summary: 'Failed',
            //    //  detail: 'Failed to Assign Technician!'
            //    //});
            //    //this.tecLoading = false;
            //  })



          },
          err => {
            this.messageService.add({
              severity: "error",
              summary: "Failed",
              detail: `Failed to edit ${user.name} info due to server error.`
            });
          });
      })
      .catch(() => {
        this.messageService.add({
          severity: "info",
          summary: "Nothing Edited!",
          detail: `No edits saved to ${user.name} info`
        });
      });
  }

  changeUserPassword(user) {
    let userCopy = Object.assign({}, user);
    this.openModalChangePassword(userCopy, `Change ${userCopy.name} password`);

  }

  lockUnlock(user, str: string) {
    Swal.fire({
      title: `Are you sure to ${str} it?`,
      text: "You won't be able to revert this!",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: `Yes, ${str} it!`
    }).then((result) => {
      if (result.value) {
        this.lookup.lockUnlockUser(user.userName).subscribe(
          () => {
            this.messageService.add({
              severity: "success",
              summary: "Successful!",
              detail: ""
            });
            user.isLock = true;


            this.users.forEach(mapedUser => {
              if (user.id === mapedUser.id) {
                return user.isLocked = !user.isLocked;
              }
            });

          },
          err => {
            this.messageService.add({
              severity: "error",
              summary: "Failed!",
              detail: ""
            });
          });
      }
    });


  }

  remove(user) {
    Swal.fire({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!'
    }).then((result) => {
      if (result.value) {
        this.lookup.removeUser(user.userName).subscribe(
          () => {
            this.messageService.add({
              severity: "success",
              summary: "Successful!",
              detail: "User deleted Successfully!"
            });

            this.users = this.users.filter((oneUser) => {
              return oneUser.userName !== user.userName;
            });
          },
          err => {
            this.messageService.add({
              severity: "error",
              summary: "Failed!",
              detail: "Failed to delete user due to network error, please try again later."
            });
          });
      }
    });
  }

  add() {
    this.openAddEditUserModal({}, `Add new user`);

    this.modalRef.result.then(
      (newUser) => {
        let userRoles = [];

        userRoles.push(newUser.roleNames);
        newUser.roleNames = userRoles;

        if (newUser.email.length === 0) {
          newUser.email = `${newUser.userName}@domain.com`;
        }

        this.lookup.postNewUser(newUser).subscribe(
          (resUser) => {
            if (resUser.roleNames[0] == 'Technican') {
              let tecWithDis = {
                fK_technican_id: resUser.id,
                fK_dispatcher_id: newUser.selectedDispatureid
              };

              this.lookup.postTecnichanWithDis(tecWithDis).subscribe(() => {
                this.messageService.add({
                  severity: "success",
                  summary: "saved successfully!",
                  detail: `New user ${resUser.firstName} ${resUser.middleName}${resUser.lastName} saved successfully!`
                });
                resUser.name = resUser.firstName + resUser.middleName + resUser.lastName;

                this.users.push(resUser);

                //this.cornerMessage = [];
                //this.cornerMessage.push({
                //  severity: 'success',
                //  summary: 'successfully',
                //  detail: `Technician ${this.tecToPost.firstName} ${this.tecToPost.middleName} Assigned to dispatcher ${this.disToPost.firstName} ${this.disToPost.middleName} successfully!`
                //});
                //this.tecLoading = false;
              },
                err => {
                  //this.cornerMessage = [];
                  //this.cornerMessage.push({
                  //  severity: 'error',
                  //  summary: 'Failed',
                  //  detail: 'Failed to Assign Technician!'
                  //});
                  //this.tecLoading = false;
                })


            }
            else {

              this.messageService.add({
                severity: "success",
                summary: "saved successfully!",
                detail: `New user ${resUser.firstName} ${resUser.middleName}${resUser.lastName} saved successfully!`
              });

              resUser.name = resUser.firstName + resUser.middleName + resUser.lastName;

              this.users.push(resUser);

            }


          },
          err => {
            this.messageService.add({
              severity: "error",
              summary: "Failed",
              detail: `Failed to save new user due to server error.`
            });
          });
      })
      .catch(() => {
        this.messageService.add({
          severity: "info",
          summary: "User canceled",
          detail: `New user didn't saved and canceled.`
        });
      });
  }

  /**
   * user modal
   * 
   *  
   * @param data 
   * @param header 
   */
  openAddEditUserModal(data, header) {
    this.modalRef = this.modalService.open(AddEditUserModalComponent, { backdrop: 'static' });
    this.modalRef.componentInstance.header = header;
    // this.modalRef.componentInstance.type = 'prompt';
    this.modalRef.componentInstance.data = data;
  }

  /**
   * change password modal
   * 
   * 
   * @param data 
   * @param header 
   */
  openModalChangePassword(data, header) {
    this.modalRef = this.modalService.open(AdminChangePasswordComponent, { backdrop: 'static' });
    this.modalRef.componentInstance.header = header;
    this.modalRef.componentInstance.data = data;
  }



  exportCsv() {
    let exportData = [];

    exportData.push({
      'Name': 'Name',
      'UserName': 'User Name',
      'Email': 'Email',
      'Phone': 'Phone',
      'Roles': 'Roles',
    });

    this.users.map((item) => {
      exportData.push({
        'Name': item.name,
        'UserName': item.userName,
        'Email': item.email,
        'Phone': item.phoneNumber,
        'Roles': this.converArrayToString(item.roleNames)
      })
    });
    return new Angular2Csv(exportData, 'Users', {
      showLabels: true
    });
  }

  /**
   * 
   * @param array 
   */
  converArrayToString(array = []): string {
    return array.join();
  }

  filter() {
    if (this.searchString.length == 0) {
      this.users = this.tempUsers;
    } else if (typeof this.searchString === 'string') {
      this.users = this.filterPipe.transform(this.users, 'name', this.searchString);
    } else if (typeof parseInt(this.searchString) === 'number') {
      this.users = this.filterPipe.transform(this.users, 'phoneNumber', this.searchString);
    }
  }

  reset() {
    this.users = this.tempUsers;
  }

}
