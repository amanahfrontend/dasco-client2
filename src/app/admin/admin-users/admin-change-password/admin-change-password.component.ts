import { Component, Input, OnInit } from "@angular/core";
import { NgbActiveModal } from "@ng-bootstrap/ng-bootstrap";
import { UserChangePassword } from "../../models/user-change-password";
import { UserService } from "../../services/user.service";

@Component({
  selector: "app-admin-change-password",
  templateUrl: "./admin-change-password.component.html",
  styleUrls: ["./admin-change-password.component.css"]
})
export class AdminChangePasswordComponent implements OnInit {

  @Input() data: any;

  model: UserChangePassword;
  toggleLoading;
  header;
  passwordPattern: any;

  constructor(
    public activeModal: NgbActiveModal,
    private readonly userService: UserService) { }

  ngOnInit() {
    this.passwordPattern = "^(?=.*[A-Za-z])(?=.*\\d)(?=.*[@$!%*#?&])[A-Za-z\\d@$!%*#?&]{8,}$";
    this.model = new UserChangePassword();
    this.model.modifiedByUserId = JSON.parse(localStorage.getItem("currentUser")).id;
    this.model.id = this.data.id;// User Id to change password for.
  }

  saveUserPassword() {

    this.userService.updateUserPassword(this.model).subscribe(
      userRes => {

        if (userRes["ok"] === true) {
          this.close();
        }
      });
  }

  close() {
    this.activeModal.close();
  }

  getLocalStorage(key) {
    return localStorage.getItem(key);
  }
}
