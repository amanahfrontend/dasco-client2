import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CustomerDropdownComponent } from './customer-dropdown.component';
import { AutoCompleteModule } from 'primeng/primeng';
import { FormsModule } from '@angular/forms';
@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    AutoCompleteModule
  ],
  declarations: [
    CustomerDropdownComponent
  ],
  exports: [
    CustomerDropdownComponent
  ]
})
export class CustomerDropdownModule { }
