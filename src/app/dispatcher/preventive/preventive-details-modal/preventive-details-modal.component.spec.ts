import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PreventiveDetailsModalComponent } from './preventive-details-modal.component';

describe('PreventiveDetailsModalComponent', () => {
  let component: PreventiveDetailsModalComponent;
  let fixture: ComponentFixture<PreventiveDetailsModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PreventiveDetailsModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PreventiveDetailsModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
