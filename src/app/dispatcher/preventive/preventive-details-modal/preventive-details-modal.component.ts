import { Component, OnInit, Input, AfterContentInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { LookupService } from '../../../api-module/services/lookup-services/lookup.service';
import Swal from 'sweetalert2'

declare let google: any;

interface Contract {
  area: string;
  contractNumber: string;
  createdDate: Date;
  deletedDate: Date;
  endDate: Date;
  fK_ContractType_Id: number;
  fK_Customer_Id: number;
  hasPreventiveMaintainence: boolean;
  id: number
  isDeleted: boolean;
  price: number;
  startDate: Date;
  updatedDate: Date;
}

interface Location {
  addressNote: string;
  area: string;
  block: string;
  createdDate: Date;
  deletedDate: Date;
  governorate: string;
  id: number;
  isDeleted: boolean;
  latitude: number;
  longitude: number;
  paciNumber: number;
  street: string;
  title: string;
  updatedDate: Date;
}

interface Customer {
  addressNote: string;
  area: string;
  block: string;
  civilId: number;
  governorate: string
  id: number;
  isDeleted: boolean;
  latitude: number;
  longitude: number;
  name: string;
  paciNumber: number;
  phoneNumber: number;
  remarks: null
  street: string;
  title: string;
  updatedDate: Date;
  createdDate: Date;
  deletedDate: Date;
}

interface Order {
  contractNumber: string;
  customer: Customer;
  contract: Contract;
  id: number;
  fK_Order_Id: number;
  isDeleted: boolean;
  location: Location;
  orderDate: Date;
  createdDate: Date;
  updatedDate: Date;
}

@Component({
  selector: 'app-preventive-details-modal',
  templateUrl: './preventive-details-modal.component.html',
  styleUrls: ['./preventive-details-modal.component.css'],
})

export class PreventiveDetailsModalComponent implements AfterContentInit {

  @Input() data: any;
  date;

  toggleLoading: boolean;
  orderProgress: any;

  lng: number = 48.03139678286879;
  lat: number = 29.270176621002467;
  map;

  address;
  progress: boolean = false;
  
  constructor(public activeModal: NgbActiveModal, private lookup: LookupService) {
  }

  ngAfterContentInit() {
    this.progress = true;
    this.lookup.getOrderProgress(this.data['data'].id).subscribe((progress) => {
      this.progress = false;
      if (progress && progress.length > 0) {
        this.orderProgress = progress;
      }
    }, errors => {
      this.progress = false;
    });
  }


  mapReady() {
  }

  close() {
    this.activeModal.dismiss();
  }

  /**
   * 
   * @param lat 
   * @param lng 
   */
  getAddress(lat: number, lng: number) {

    if (lat == null || lng == null) {
      this.showAddress('not-found');
    }

    if (navigator.geolocation) {

      let geocoder = new google.maps.Geocoder();
      let latlng = new google.maps.LatLng(lat, lng);
      let request = { latLng: latlng };

      geocoder.geocode(request, (results, status) => {
        if (status === google.maps.GeocoderStatus.OK) {
          let result = results[0];
          let rsltAdrComponent = result.address_components;
          if (result != null) {
            this.address = rsltAdrComponent;
            this.showAddress('found');
          } else {
            alert('No address available!');
          }
        }
      });
    }
  }

  /**
   * 
   * @param addresses 
   */
  joinAddressString(addresses = []) {
    let address = '';
    for (let index = 0; index < addresses.length; index++) {
      if (addresses[index] != undefined) {
        address += addresses[index].long_name + ', ';
      }
    }
    return address;
  }

  /**
   * 
   * @param str 
   */
  showAddress(str: string) {

    switch (str) {
      case 'found':
        Swal.fire({
          type: 'info',
          html:
            `<h4>${this.joinAddressString(this.address)}</h4>`,
          showCloseButton: true,
        });
        break;

      case 'not-found':
        Swal.fire({
          type: 'info',
          html:
            `<h4>Address does not exist</h4>`,
          showCloseButton: true,
        });
        break;

      default:
        break;
    }
  }

}
