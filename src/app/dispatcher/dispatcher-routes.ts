import { DispatcherOrderBoardComponent } from './dispatcher-order-board/dispatcher-order-board.component'
import { AuthGuardGuard } from "../api-module/guards/auth-guard.guard";
import { ReportComponent } from "./report/report.component";
import { PreventiveComponent } from './preventive/preventive/preventive.component';
import { VisitTimeCalenderComponent } from "./visit-time-calender/visit-time-calender.component";
import { RoleGuard } from './../api-module/guards/role.guard';

export const dispatcherRoutes = [
  {
    path: '',
    component: DispatcherOrderBoardComponent,
    canActivate: [RoleGuard],
    data: { roles: ['Dispatcher', 'SupervisorDispatcher'] }
  },
  {
    path: 'report',
    component: ReportComponent,
    canActivate: [AuthGuardGuard],
    data: { roles: ['RoleGuard', 'SupervisorDispatcher', 'Admin'] }
  },
  {
    path: 'calender',
    component: VisitTimeCalenderComponent,
    canActivate: [RoleGuard],
    data: { roles: ['Dispatcher', 'SupervisorDispatcher'] }
  },
  {
    path: 'preventive',
    component: PreventiveComponent,
    canActivate: [RoleGuard],
    data: { roles: ['Dispatcher', 'SupervisorDispatcher'] }
  }
];
