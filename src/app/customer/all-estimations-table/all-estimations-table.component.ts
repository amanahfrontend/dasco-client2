import { Component, Input, OnInit } from '@angular/core';
import { UtilitiesService } from "../../api-module/services/utilities/utilities.service";
import { Router } from "@angular/router";
import { Subscription } from "rxjs/Subscription";
import { LookupService } from "../../api-module/services/lookup-services/lookup.service";
import { Message } from "primeng/components/common/api";
import Swal from 'sweetalert2';

@Component({
  selector: 'app-all-estimations-table',
  templateUrl: './all-estimations-table.component.html',
  styleUrls: ['./all-estimations-table.component.css']
})

export class AllEstimationsTableComponent implements OnInit {
  @Input() estimations: any[];

  buttonsList: any[];
  activeRow: any;
  deleteEstimationSubscription: Subscription;
  cornerMessage: Message[] = [];

  constructor(private utilities: UtilitiesService, private router: Router, private lookup: LookupService) {
  }

  ngOnInit() {

  }

  /**
   * 
   * @param id 
   */
  removeEstimation(id: number) {
    Swal.fire({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!'
    }).then((result) => {
      if (result.value) {
        this.deleteEstimationSubscription = this.lookup.deleteEstimation(id).subscribe(() => {
          this.estimations = this.estimations.filter((estimation) => {
            return estimation.id != id;
          });
          this.cornerMessage.push({
            severity: 'success',
            summary: 'Success!',
            detail: 'Estimation removed successfully!'
          });
        },
          err => {
            err.status == 401 && this.utilities.unauthrizedAction();
            this.cornerMessage.push({
              severity: 'error',
              summary: 'Failed',
              detail: 'Failed to remove Estimation due to server error!'
            });
          });

      }
    });

  }

  editEstimation(row) {
    console.log(this.utilities.currentSearch);
    this.router.navigate(['/search/editEstimation/', row.id]);
  }

  routeToEstimationDetails(estimationHeader) {
    this.utilities.setRoutingDataPassed(estimationHeader);
    // let estimation = JSON.stringify(estimationHeader);
    this.router.navigate(['/search/estimation/', estimationHeader.id]);
  }
}
