import { Component, OnInit, Input } from '@angular/core';
import { NgbActiveModal } from "@ng-bootstrap/ng-bootstrap";
import { LookupService } from "../../api-module/services/lookup-services/lookup.service";
import { Message } from "primeng/components/common/message";

@Component({
  selector: 'app-edit-order-modal',
  templateUrl: './edit-order-modal.component.html',
  styleUrls: ['./edit-order-modal.component.css']
})
export class EditOrderModalComponent implements OnInit {
  @Input() data: any;
  @Input() notificationOrder: boolean;
  modalMessage: Message[] = [];

  isCashCall: boolean;

  constructor(public activeModal: NgbActiveModal, private lookup: LookupService) {
  }

  ngOnInit() {
    let previousOrderType: string = this.data.orderType.name;    
    switch (previousOrderType) {
      case 'Cash Call':
        this.isCashCall = true;
        break;
      default:
        this.isCashCall = false;
        break;
    }    
  }

  updateOrder(order) {
    console.log("Updated Order ", order);

    order.id = this.data.id;
    order.code = this.data.code;
    order.fK_Contract_Id = this.data.fK_Contract_Id;
    order.fK_Technican_Id = this.data.fK_Technican_Id;
    order.fK_Dispatcher_Id = this.data.fK_Dispatcher_Id;
    order.fK_Location_Id = this.data.fK_Location_Id;
    order.fK_Customer_Id = this.data.fK_Customer_Id;
    order.fK_Customer_Id = this.data.fK_Customer_Id;
    order.fk_Call_Id = this.data.fk_Call_Id;
    order.fK_OrderType_Id = this.data.fK_OrderType_Id;
    order.area = this.data.area;
    order.startDate = this.data.startDate;
    order.note = this.data.note;

    order.createdDate = this.data.createdDate;

    order.quotationRefNo = this.data.quotationRefNo;
    order.price = this.data.price;
    order.fK_OrderStatus_Id = this.data.fK_OrderStatus_Id;


    order.preferedVisitTime = this.data.preferedVisitTime;




    console.log("Updated Order ", order);
    console.log("Updated Data ", this.data);

    this.lookup.updateOrder(order).subscribe(() => {


      console.log("Updated Order ", order);

      this.modalMessage.push({
        severity: "success",
        summary: "Order Saved Successfully",
        detail: "Order Edited and saved successfully!"
      });


      this.activeModal.close(order);
    },
      err => {
        this.modalMessage.push({
          severity: "error",
          summary: "Failed",
          detail: "Failed to save order due to network error!"
        });

      })
  }

}
