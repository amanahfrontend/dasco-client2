import {Component, OnInit, ViewEncapsulation, OnDestroy} from '@angular/core';
import {LookupService} from "../../api-module/services/lookup-services/lookup.service";
import {ActivatedRoute, Params, Router} from "@angular/router";
import {MessageService} from "primeng/components/common/messageservice";
import {Message} from "primeng/components/common/message";
import {UtilitiesService} from "../../api-module/services/utilities/utilities.service";

@Component({
  selector: 'app-generate-contract',
  templateUrl: './generate-contract.component.html',
  styleUrls: ['./generate-contract.component.css'],
  encapsulation: ViewEncapsulation.None
})

export class GenerateContractComponent implements OnInit, OnDestroy {
  quotationDetails: any;
  customerDetails: any;
  subscribtionQuotationDetails: any;
  value: Date;
  toggleLoading: boolean;
  cornerMessage: Message[];

  constructor(private activeRoute: ActivatedRoute, private lookup: LookupService, private messageService: MessageService, private utilities: UtilitiesService) {
  }

  ngOnInit() {
    this.toggleLoading = true;

    this.activeRoute.params.forEach((param: Params) => {
      this.subscribtionQuotationDetails = this.lookup.getQuotationDetailsByRefNo(param['refNumber']).subscribe((quotationDetails) => {
          this.quotationDetails = quotationDetails;
          this.customerDetails = {
            customerName: this.quotationDetails.call.callerName,
            customerMobile: this.quotationDetails.call.callerNumber,
            area: this.quotationDetails.call.area
          };
          this.toggleLoading = false;
        },
        (err) => {
          this.cornerMessage.push({
            severity: 'error',
            summary: 'Failed!',
            detail: 'Failed to get data due to network error, please try again later.'
          });
          this.toggleLoading = false;
        })
    });
  }

  ngOnDestroy() {
    this.utilities.routingFromAndHaveSearch = true;
    this.subscribtionQuotationDetails.unsubscribe();
  }


}
