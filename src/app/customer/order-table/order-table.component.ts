import {Component, OnInit, Input, OnChanges, OnDestroy, Output, EventEmitter} from '@angular/core';
import {EditOrderModalComponent} from "../edit-order-modal/edit-order-modal.component"
import {Subscription} from "rxjs";
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {Message} from 'primeng/components/common/api';
import {LookupService} from "../../api-module/services/lookup-services/lookup.service";
import {UtilitiesService} from "../../api-module/services/utilities/utilities.service";
import { OrderModalStatusComponent } from '../order-modal-status/OrderModalStatus.component';
import { Angular2Csv } from "angular2-csv";
import { Page } from '../../shared-module/shared/model/page';


@Component({
  selector: 'app-order-table',
  templateUrl: './order-table.component.html',
  styleUrls: ['./order-table.component.css']
})

export class OrderTableComponent implements OnInit, OnChanges, OnDestroy {
  @Input() orders: any[];
  @Output() emitAllData: EventEmitter<any> = new EventEmitter<any>();
  @Output() emitSearchedData: EventEmitter<any> = new EventEmitter<any>();

  @Input() isCustomerDetails: boolean= false;

  deleteOrderSubscription: Subscription;
  cornerMessage: Message[] = [];
  modalRef: any;
  buttonsList: any[];
  activeRow: any;
  @Input() page: Page;

  constructor(private lookup: LookupService, private modalService: NgbModal, private utilities: UtilitiesService) {
  }

  ngOnInit() {
    // this.orders = [];
    this.buttonsList = [
      {
        label: "Remove", icon: "fa fa-times", command: () => {
        this.remove(this.activeRow.id)
      }
      }
    ];
  }

  ngOnChanges() {
    //console.log(this.orders);
    this.orders = this.orders || [];
    this.orders.map((order) => {
      order.startDateView = this.utilities.convertDatetoNormal(order.startDate);

      order.endDateView = this.utilities.convertDatetoNormal(order.endDate);
    });
  }
  setPage(pageInfo) {
    this.page.pageNumber = pageInfo.offset;
    if (this.utilities.paginationFlag == false)
      this.emitAllData.emit(this.page.pageNumber);
    else
      this.emitSearchedData.emit({ pageNumber: this.page.pageNumber, searchedParmaters: 'search' });
  }



    exportCsv() {
    let exportData = [];
    exportData.push({
      'Order Number ': 'Order Number',
      'Order Problem ': 'Order Problem ',
      'Order Priority ': 'Order Priority ',
      'Start date': 'Start date',
      'End Date': 'End Date',
      'Price': 'Price',
    });
      this.orders.map((item) => {
        if (item.problem == null)
          item.orderProblem = { "name": "-" }
        if (item.orderPriority == null)
          item.orderPriority = {"name":"-"}
      exportData.push({
        'Order Number': (item.code == null) ? '' : item.code,
        'Order Problem': (item.orderProblem.name == null) ? '' : item.orderProblem.name,
        'Order Priority': (item.orderPriority.name == null) ? '' : item.orderPriority.name,
        'Start date': (item.startDate == null) ? '' : item.startDate,
        'End Date': (item.endDate == null) ? '' : item.endDate,
        'Price': (item.price == null) ? '' : item.price,

      })
    });
    return new Angular2Csv(exportData, 'OrdersReport', {
      showLabels: true
    });
  }


  ngOnDestroy() {
    this.deleteOrderSubscription && this.deleteOrderSubscription.unsubscribe();
  }

  setActiveRow(row) {
    this.activeRow = row;
  }

  edit(order) {
    this.openModal(order);
    this.modalRef.result
      .then(() => {
        //this.cornerMessage.push({
        //  severity: "success",
        //  summary: "Order Saved!",
        //  detail: "Order edits applied & Saved successfully."
        //})
      })
      .catch(() => {
        //this.cornerMessage.push({
        //  severity: "info",
        //  summary: "Cancelled!",
        //  detail: "Order Edits cancelled without saving."
        //})
      })
  }


  viewOrderStatus(order) {
    this.openModalStatus(order);
    this.modalRef.result
      .then(() => {
        
      })
     
  }


  openModal(data) {
    console.log(data);
    this.modalRef = this.modalService.open(EditOrderModalComponent,{backdrop:'static'});
    this.modalRef.componentInstance.data = data;

  }

  openModalStatus(data) {
    this.modalRef = this.modalService.open(OrderModalStatusComponent,{backdrop:'static'});
    this.modalRef.componentInstance.data = data;
  }

  remove(id) {
    this.deleteOrderSubscription = this.lookup.deleteOrder(id).subscribe(() => {
        this.cornerMessage.push({
          severity: "success",
          summary: "Removed successfully",
          detail: "Order removed successfully."
        });
        this.orders = this.orders.filter((order) => {
          return order.id != id;
        })
      },
      err => {
        this.cornerMessage.push({
          severity: "error",
          summary: "Failed",
          detail: "Failed to get data dut to network error, please try again later."
        })
      })
  }



}
