import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { LookupService } from "../../api-module/services/lookup-services/lookup.service";
import { Angular2Csv } from "angular2-csv";
import { UtilitiesService } from "../../api-module/services/utilities/utilities.service";
import { AuthenticationServicesService } from '../../api-module/services/authentication/authentication-services.service';
import { Page } from '../../shared-module/shared/model/page';

@Component({
  selector: 'app-customer-list',
  templateUrl: './customer-list.component.html',
  styleUrls: ['./customer-list.component.css']
}) 
export class CustomerListComponent implements OnInit {
  customers: any[] = [];
  allCustomers: any[] = [];
  toggleLoading: boolean;
  hideDate: boolean = false;
  filteredCustomers: any[] = [];
  s: string;
  roles: string[];
  rows = new Array<any>();

  page: Page=new Page();
  paginationFlag: boolean = false;
  @Output() emitSearchedData: EventEmitter<any> = new EventEmitter<any>();
  searchedString: any;
  totalElements: number = 0;

  constructor(private lookup: LookupService, private utilities: UtilitiesService, private auth: AuthenticationServicesService) {
    this.page.pageNumber = 1;
    this.page.pageSize = 10;
  }

  ngOnInit() {
    this.toggleLoading = true;
    this.roles = this.auth.CurrentUser().roles;

    this._getCustomers({ offset: 0 });


  }
  setPage(pageInfo) {
    this.page.pageNumber = pageInfo.offset;
    if (this.paginationFlag == false)
      this._getCustomers(this.page.pageNumber);
    else
      this.searchByValue(this.searchedString);
  }

  _getCustomersAllCustomers() {
    this.paginationFlag = false
    this.searchedString = null;
    this._getCustomers({ offset: 0 });
  }

  _getCustomers(pageNumber: any) {
    this.toggleLoading = true;
    if (pageNumber > 0)
      this.page.pageNumber = pageNumber;
    else
      this.page.pageNumber = 0;


    let objToPost = Object.assign({}, this.page);
    objToPost.pageNumber = this.page.pageNumber + 1;



    this.lookup.getAllCustomersByPaging(objToPost)
      .subscribe((customers) => {
        this.rows = customers.result;
        this.totalElements = customers.totalCount;

        this.customers = customers.result;
        this.allCustomers = JSON.parse(JSON.stringify(this.customers));
        this.filteredCustomers = this.customers;

        this.toggleLoading = false;
      },
        err => {
          this.toggleLoading = false;
        })
  }

  //filterQustomers(searchData) {
  //  this.paginationFlag = true;
  //  if (searchData.searchText != undefined)
  //    this.searchedString = searchData.searchText;
  //  else
  //    this.searchedString = searchData;

  //  this.filteredCustomers = [];
  //  for (let i = 0; i < this.customers.length; i++) {
  //    if (this.customers[i].name) {
  //      if (this.customers[i].name.toLowerCase().includes(this.searchedString.trim().toLowerCase())) {
  //        this.filteredCustomers.push(this.customers[i]);
  //      }
  //    }
  //    if (this.customers[i].civilId) {
  //      if (this.customers[i].civilId.toLowerCase().includes(searchedString.trim().toLowerCase())) {
  //        this.filteredCustomers.push(this.customers[i]);
  //      }
  //    }
  //    if (this.customers[i].customerPhoneBook[0].phone) {
  //      if (this.customers[i].customerPhoneBook[0].phone.toLowerCase().includes(searchedString.trim().toLowerCase())) {
  //        this.filteredCustomers.push(this.customers[i]);
  //      }
  //    }
  //    if (this.customers[i].locations[0].block) {
  //      if (this.customers[i].locations[0].block.toLowerCase().includes(searchedString.trim().toLowerCase())) {
  //        this.filteredCustomers.push(this.customers[i]);
  //      }
  //    }
  //    if (this.customers[i].locations[0].street) {
  //      if (this.customers[i].locations[0].street.toLowerCase().includes(searchedString.trim().toLowerCase())) {
  //        this.filteredCustomers.push(this.customers[i]);
  //      }
  //    }
  //    if (this.customers[i].locations[0].area) {
  //      if (this.customers[i].locations[0].area.toLowerCase().includes(searchedString.trim().toLowerCase())) {
  //        this.filteredCustomers.push(this.customers[i]);
  //      }
  //    }
  //    if (this.customers[i].locations[0].governorate) {
  //      if (this.customers[i].locations[0].governorate.toLowerCase().includes(searchedString.trim().toLowerCase())) {
  //        this.filteredCustomers.push(this.customers[i]);
  //      }
  //    }
  //  }
  //  console.log('%c we are here', 'color: orange; font-weight: bold;');
  //  console.log(this.filteredCustomers);
  //}


  searchByValue(searchData) {
    this.toggleLoading = true;
    this.paginationFlag = true;
    this.filteredCustomers = [];

    if (searchData.searchText != undefined)
      this.searchedString = searchData.searchText;
    else
      this.searchedString = searchData;

    let pageNumber = this.page.pageNumber;
    if (pageNumber > 0)
      this.page.pageNumber = pageNumber;
    else
      this.page.pageNumber = 0;

    let objToPost = Object.assign({}, this.page);
    objToPost.pageNumber = this.page.pageNumber + 1;
    objToPost.searchBy = this.searchedString;


    this.lookup.searchCustmerByPaging(objToPost).subscribe((searchResult) => {
      this.filteredCustomers = searchResult.result;
      this.totalElements = searchResult.totalCount;
      this.toggleLoading = false;
    },
      err => {
        this.toggleLoading = false;
      })
  }


  printCustomer() {
    this.utilities.printComponent('customer-table')
  }

  exportCsv() {
    let exportData = [];

    this.customers.map((item) => {
      exportData.push({
        'Name': (item.name == null) ? '' : item.name,
        'Phone': (item.customerPhoneBook[0].phone == null) ? '' : item.customerPhoneBook[0].phone,
        'civilId': (item.civilId == null) ? '' : item.civilId,
        'Address': `${item.locations[0].block}, ${item.locations[0].street}, ${item.locations[0].area}, ${item.locations[0].governorate}`
      })
    });

    return new Angular2Csv(exportData, 'Customers', {
      showLabels: true,
      headers: ['Name', 'Phone', 'CID', 'Address'],
      showTitle: true,
      keys: ['Name', 'Phone', 'civilId', 'Address'],
      removeNewLines: true,
    });
  }

}
