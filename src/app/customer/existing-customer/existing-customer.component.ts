// import {
//   customer,
//   contract,
//   complain,
//   CustomerHistory,
//   WorkOrderDetails
// } from './../../api-module/models/customer-model';
import {Subscription} from 'rxjs/Subscription';
import {AlertServiceService} from './../../api-module/services/alertservice/alert-service.service';
import {CustomerCrudService} from './../../api-module/services/customer-crud/customer-crud.service';
import {Router, ActivatedRoute} from '@angular/router';
import {Component, OnInit, OnDestroy} from '@angular/core';
import {UtilitiesService} from "../../api-module/services/utilities/utilities.service";
import {LookupService} from "../../api-module/services/lookup-services/lookup.service";
import { Page } from '../../shared-module/shared/model/page';

@Component({
  selector: 'app-existing-customer',
  templateUrl: './existing-customer.component.html',
  styleUrls: ['./existing-customer.component.css']
})

export class ExistingCustomerComponent implements OnInit, OnDestroy {
  // public selectedTab = 'tab1';
  public existCust = false;
  private callId: number;
  private customerId: number;
  private sub: any;
  existedOrders: any[];
  existedCalls: any[];
  callData: any;
  existedCustomerData: any;
  workOrders;
  activeTab: string;
  orderByCustomerSubscription: Subscription;
  callByCustomerSubscription: Subscription;
  toggleLoading: boolean;
  page: Page = new Page();

  public custDetails = true;

  constructor(private router: Router, private route: ActivatedRoute,
    private customerService: CustomerCrudService, private alertService: AlertServiceService, private utilities: UtilitiesService, private lookupService: LookupService) {
    this.page.pageNumber = 1;
    this.page.pageSize = 10;

  }

  ngOnInit() {
    this.callData = {};
    // //console.log('existed customer component');
    this.existedOrders = [];
    this.existedCalls = [];
    this.sub = this.route.params.subscribe(params => {
      if (params['customerId']) {
        this.customerId = +params['customerId'];
        console.log('find customer')
        this.getCustomerDetails(this.customerId);
      } else {
        console.log('find call')
        this.callId = +params['CurrentCustomer'];
        this.GetCallDetails();
      }
    });
    this.activeTab = 'info';
  }



  ngOnDestroy() {
    this.sub.unsubscribe();
    this.orderByCustomerSubscription && this.orderByCustomerSubscription.unsubscribe();
    this.callByCustomerSubscription && this.callByCustomerSubscription.unsubscribe();
  }

  setActiveTab(tab) {
    this.activeTab = tab;
    if (tab == 'orders') {
      this.getOrderDetails({ offset: 0 });
    } else if (tab == 'calls') {
      this.getCallsDetailsByPaging({ offset: 0 });
    }
  }
  getOrderDetails(pageNumber) {

    if (pageNumber > 0)
      this.page.pageNumber = pageNumber;
    else
      this.page.pageNumber = 0;


    let objToPost = Object.assign({}, this.page);
    objToPost.pageNumber = this.page.pageNumber + 1;
    objToPost.id = this.customerId;

    this.orderByCustomerSubscription = this.lookupService.getOrderByCustomerIdWithPaging(objToPost).subscribe((orders) => {
      this.page.totalElements = orders.totalCount;
      this.existedOrders = orders.result;
    },
      err => {
        //console.log(err);
      })
  }


  getCallsDetailsByPaging(pageNumber) {

    if (pageNumber > 0)
      this.page.pageNumber = pageNumber;
    else
      this.page.pageNumber = 0;


    let objToPost = Object.assign({}, this.page);
    objToPost.pageNumber = this.page.pageNumber + 1;
    objToPost.id = this.customerId;

    this.callByCustomerSubscription = this.lookupService.getCallByCustomerIdBypaging(objToPost).subscribe((calls) => {
      this.page.totalElements = calls.totalCount;
      this.existedCalls = calls.result;
    },
      err => {
        //console.log(err);
      })
  }


  getCustomerDetails(customerId) {
    console.log(this.callData.fK_Customer_Id);
    this.lookupService.getCustomerDetails(customerId).subscribe((existedCustomerData) => {
        this.existedCustomerData = existedCustomerData;
        console.log(this.existedCustomerData);
      },
      err => {
        console.log(err);
      })
  }

  GetCallDetails() {
    //console.log('will get customer details')
    this.toggleLoading = true;
    this.customerService.GetCallDetail(this.callId).subscribe(callData => {
        this.callData = callData;
        console.log(this.callData);
        if (this.callData.fK_Customer_Id) {
          this.customerId = this.callData.fK_Customer_Id;
          this.getCustomerDetails(this.customerId);
        } else {
          this.utilities.updateCurrentExistedCustomer(callData);
        }
        this.toggleLoading = false;
      },
      err => {
        this.alertService.error('we have Server Error !')
      });
  }


}
