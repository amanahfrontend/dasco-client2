import {
  Component,
  OnInit,
  OnDestroy,
  Input,
  OnChanges,
  EventEmitter,
  Output,
} from '@angular/core';
import { Message } from "primeng/components/common/message";
import { Subscription } from "rxjs";
import { Router, ActivatedRoute } from "@angular/router";
import { AuthenticationServicesService } from "../../api-module/services/authentication/authentication-services.service";
import { LookupService } from "../../api-module/services/lookup-services/lookup.service";
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Angular2Csv } from "angular2-csv";
import { UtilitiesService } from "../../api-module/services/utilities/utilities.service";
import Swal from 'sweetalert2';
import { Page } from '../../shared-module/shared/model/page';
import { CustomerCrudService } from '../../api-module/services/customer-crud/customer-crud.service';
@Component({
  selector: 'app-call-table',
  templateUrl: './call-table.component.html',
  styleUrls: ['./call-table.component.css'],
})

export class CallTableComponent implements OnInit, OnDestroy, OnChanges {

  @Input() calls;
  @Input() page: Page;
  @Output() emitAllData: EventEmitter<any> = new EventEmitter<any>();
  @Output() emitSearchedData: EventEmitter<any> = new EventEmitter<any>();


  buttonsList: any[];
  cornerMessage: Message[] = [];
  statusSubscription: Subscription;
  roles: string[];
  content: any;
  activeRow: any;
  newAction: any;
  statuses: any[];
  modalRef: any;
  totalElements: any = 30;
  temp = [];
  sideCalls = [];
  count: number = 0;
  selectedElement;

  constructor(private customerService: CustomerCrudService, private lookUp: LookupService, private modalService: NgbModal, private router: Router, private auth: AuthenticationServicesService, private activatedRoute: ActivatedRoute, private utilitiesService: UtilitiesService) {
  }


  ngOnChanges() {
    if (this.calls) {
      this.sideCalls = this.calls;
      this.calls = this.calls
      this.temp = this.calls;
      this.count = this.page.totalElements;
      this.selectedElement = 'Filter by status';
    }

  }

  ngOnInit() {
    this.calls = [];
    this.roles = this.auth.CurrentUser().roles;
    this.getStatuses();
  }

  setPage(pageInfo) {
    this.page.pageNumber = pageInfo.offset;
    if (this.customerService.paginationFlag == false)
      this.emitAllData.emit(this.page.pageNumber);
    else
      this.emitSearchedData.emit({ pageNumber: this.page.pageNumber, searchedParmaters: 'search' });
  }

  ngOnDestroy() {
    this.statusSubscription && this.statusSubscription.unsubscribe();

  }
  // { { row.block } } { { row.street } }, { { row.area } }, { { row.governorate } }

  exportCsv() {
    let exportData = [];
    exportData.push({
      'Caller Name': 'Caller Name',
      'Caller Number': 'Caller Number',
      'Call Status': 'Call Status',
      'Governarate': 'Governarate',
      'Area': 'Area',
    });
    this.calls.map((item) => {
      exportData.push({
        'Caller Name': (item.callerName == null) ? '' : item.callerName,
        'Caller Number': (item.callerNumber == null) ? '' : item.callerNumber,
        'Call Status': (item.callStatus == null) ? '' : item.callStatus,
        'Governarate': (item.governorate == null) ? '' : item.governorate,
        'GovernarAreaate': (item.area == null) ? '' : item.area,
      })
    });
    return new Angular2Csv(exportData, 'All Calls almost done', {
      showLabels: true
    });
  }

  setActiveRow(call, content) {
    this.activeRow = call;
    this.content = content;
  }


  convertToCustomer(callData: any) {
    this.router.navigate(['search/newCustomer/', callData.id])
  }

  getStatuses() {
    this.statusSubscription = this.lookUp.getStatues().subscribe((status) => {
      this.statuses = status;
    },
      err => {
        this.cornerMessage.push({
          severity: "error",
          summary: "Failed",
          detail: "Failed to get data due to server error"
        })
      })
  }


  routeToGenerateOrder(id) {
    // let contractStringfied = JSON.stringify(contract);
    this.router.navigate(['search/caller/', "generateOrder", id]);
  }

  open(call, content) {
    this.setActiveRow(call, content);

    this.newAction = {};


    this.modalService.open(this.content).result
      .then((result) => {
        if (result) {
          result.fk_Call_Id = call.id;
          result.customerServiceName = this.auth.CurrentUser().fullName;
          result.customerServiceUserName = this.auth.CurrentUser().userName;

          this.lookUp.postNewLog(result).subscribe(() => {
            //console.log('posted successfully');
            this.cornerMessage.push({
              severity: "success",
              summary: "Saved successfully",
              detail: "Action Saved to this call Successfully."
            })
          },
            err => {
              this.cornerMessage.push({
                severity: "error",
                summary: "Failed",
                detail: "Failed Save new Action due to server error"
              })
            });
          //console.log(result);
        }
      });
  }

  routeToLogs(callId) {
    this.router.navigate(['search/calls-history/logs/', callId])
  }

  roueToNewEstimation(callId) {
    this.router.navigate(['search/calls-history/estimation/', callId])
  }

  remove(id: number) {
    Swal.fire({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!'
    }).then((result) => {
      if (result.value) {
        this.lookUp.deleteCall(id).subscribe(() => {
          this.calls.filter((call) => {
            return call.id != id;
          });

          this.cornerMessage.push({
            severity: 'success',
            summary: 'Success',
            detail: 'Call Deleted Successfully!'
          });
          this.calls = this.calls.filter((call) => {
            return call.id != id;
          })
        },
          err => {
            err.status == 401 && this.utilitiesService.unauthrizedAction();
            this.cornerMessage.push({
              severity: 'error',
              summary: 'Failed!',
              detail: 'Failed to delete Call due to connection error.'
            });
          });
      }
    });
  }


  /**
   * 
   * @param event 
   */
  updateFilter(name: string) {
    if(name !== 'undefined'){
      const val = name.toLowerCase();

      const temp = this.temp.filter((row) => {
        return row.callStatus.toLowerCase().indexOf(val) !== -1 || !val;
      });
  
      this.calls = temp;
      this.calls.offset = 0;
      this.count = this.calls.length;
  
      if (temp.length === 0) {
        this.cornerMessage.push({
          severity: "info",
          summary: "Empty results",
          detail: "No results found!"
        });
  
        this.calls = this.sideCalls;
        this.count = this.page.totalElements;
      }
    }
  }

  reset() {
    this.calls = this.sideCalls;
    this.count = this.page.totalElements;
    this.selectedElement = 'Filter by status';
  }
}
