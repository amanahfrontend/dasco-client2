import { Component, OnInit, OnDestroy, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { LookupService } from "../../api-module/services/lookup-services/lookup.service";
import { Subscription } from "rxjs";
import { Message } from 'primeng/components/common/api';
// import {EditOrderModalComponent} from "../edit-order-modal/edit-order-modal.component"
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { UtilitiesService } from "../../api-module/services/utilities/utilities.service";
import { Page } from '../../shared-module/shared/model/page';

@Component({
  selector: 'app-all-orders',
  templateUrl: './all-orders.component.html',
  styleUrls: ['./all-orders.component.css'],
  // changeDetection: ChangeDetectionStrategy.OnPush
})

export class AllOrdersComponent implements OnInit, OnDestroy {
  allOrdersSubscription: Subscription;
  searchSubscription: Subscription;
  allOrders: any[];
  bigMessage: Message[] = [];
  cornerMessage: Message[] = [];
  toggleLoading: boolean;
  page = new Page();
  rows = new Array<any>();

  constructor(private lookup: LookupService, private utilities: UtilitiesService) {
    this.page.pageNumber = 1;
    this.page.pageSize = 10;
  }

  ngOnInit() {
    this.getAllOrders({ offset: 0 });
  }

  getAllOrders(pageNumber) {
    this.toggleLoading = true;
    if (pageNumber > 0)
      this.page.pageNumber = pageNumber;
    else
      this.page.pageNumber = 0;


    let objToPost = Object.assign({}, this.page);
    objToPost.pageNumber = this.page.pageNumber + 1;


    this.allOrdersSubscription = this.lookup.getAllOrdersByPaging(objToPost).subscribe((allOrders) => {
      this.rows = allOrders.result;
      this.page.totalElements = allOrders.totalCount;
      this.allOrders = allOrders.result;
      this.toggleLoading = false;

      //console.log(this.allOrders);
      this.bigMessage = [];
      this.bigMessage.push({
        severity: "info",
        summary: "Dropdown arrow",
        detail: "You can remove any order by click on the drop down arrow and choose remove."
      });
    },
      err => {
        this.toggleLoading = false;
        err.status == 401 && this.utilities.unauthrizedAction();
        this.cornerMessage.push({
          severity: "error",
          summary: "Failed",
          detail: "Failed to get data due to network error, please try again later."
        })
      })
  }

  ngOnDestroy() {
    this.allOrdersSubscription.unsubscribe();
    // this.deleteOrderSubscription && this.deleteOrderSubscription.unsubscribe();
    this.searchSubscription && this.searchSubscription.unsubscribe();
  }

  searchByValue(searchText) {
    this.toggleLoading = true;
    this.utilities.paginationFlag = true;
    let pageNumber;

    if (searchText.searchedParmaters == null || searchText.searchedParmaters == undefined)
      this.utilities.searchedValue = searchText;
    else {
      pageNumber = searchText.pageNumber;
      searchText = this.utilities.searchedValue;

    }
    if (pageNumber > 0)
      this.page.pageNumber = pageNumber;
    else
      this.page.pageNumber = 0;

    let objToPost = Object.assign({}, this.page);
    objToPost.pageNumber = this.page.pageNumber + 1;
    objToPost.searchBy = searchText;


    this.searchSubscription = this.lookup.searchOrderNumberBYpaging(objToPost).subscribe((searchResult) => {
      this.allOrders = searchResult.result;
      this.page.totalElements = searchResult.totalCount;
      console.log("Search results ", this.allOrders)



      this.toggleLoading = false;
      //console.log(searchResult);
      if (!searchResult.orderViewModelList.length) {
        this.cornerMessage.push({
          severity: "error",
          summary: "Failed",
          detail: "Result not found."
        });
      }
    },
      err => {
        this.toggleLoading = false;
        this.cornerMessage.push({
          severity: "error",
          summary: "Failed",
          detail: "Failed to get data due to network error, please try again later."
        });
      })
  }

}
