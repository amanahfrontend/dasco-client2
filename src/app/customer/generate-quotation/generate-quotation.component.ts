import {Component, OnDestroy, OnInit, ViewEncapsulation} from '@angular/core';
import {ActivatedRoute, Params} from "@angular/router";
import {LookupService} from "../../api-module/services/lookup-services/lookup.service";
import {Message} from "primeng/components/common/message";
import {UtilitiesService} from "../../api-module/services/utilities/utilities.service";
// import {UtilitiesService} from "../../api-module/services/utilities/utilities.service";

@Component({
  selector: 'app-generate-quotation',
  templateUrl: 'generate-quotation.component.html',
  styleUrls: ['generate-quotation.component.css'],
  encapsulation: ViewEncapsulation.None
})

export class GenerateQuotationComponent implements OnInit, OnDestroy {
  estimationDetails: any;
  customerDetails: any;
  estimationSubscription: any;
  type: string;
  toggleLoading: boolean;
  msg: Message[];

  constructor(private activeRoute: ActivatedRoute, private lookup: LookupService, private utilities: UtilitiesService) {
  }

  ngOnInit() {
    this.toggleLoading = true;
    this.type = 'quotation';
    this.estimationDetails = [];
    this.utilities.routingFromAndHaveSearch = true;
    this.activeRoute.params.forEach((params: Params) => {
      this.estimationSubscription = this.lookup.getEstimationDetails(+params['id']).subscribe((estimationDetails) => {
          this.estimationDetails = estimationDetails;
          this.estimationDetails.lstEstimationItems.map((estimation) => {
            estimation.soldQuantity = estimation.quantity;
          });
          this.customerDetails = {
            customerName: this.estimationDetails.call.callerName,
            customerMobile: this.estimationDetails.call.callerNumber,
            area: this.estimationDetails.call.area,
            fK_Call_Id: this.estimationDetails.call.id
          };
          this.toggleLoading = false;
        },
        err => {
          this.toggleLoading = false;
        })
    })
  }

  ngOnDestroy() {
    this.utilities.routingFromAndHaveSearch = false;
  }

}
