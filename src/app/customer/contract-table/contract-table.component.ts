import { Component, OnInit, Input, OnChanges, Output, EventEmitter } from '@angular/core';
import { Subscription } from "rxjs";
import { LookupService } from "../../api-module/services/lookup-services/lookup.service";
import { Message } from "primeng/components/common/message";
import { Router } from "@angular/router";
import { UtilitiesService } from "../../api-module/services/utilities/utilities.service";
import { Angular2Csv } from "angular2-csv";
import { Page } from '../../shared-module/shared/model/page';

// import {Http} from "@angular/http";

@Component({
  selector: 'app-contract-table',
  templateUrl: './contract-table.component.html',
  styleUrls: ['./contract-table.component.css'],
})

export class ContractTableComponent implements OnInit, OnChanges {
  @Input() contracts: any;
  @Output() print = new EventEmitter();
  
  activeRow: any;
  deleteContractSubscription: Subscription;
  buttonsList: any[];
  
  cornerMessage: Message[] = [];
  todayDate: Date = new Date();
  @Input() page: Page;

  @Output() emitAllData: EventEmitter<any> = new EventEmitter<any>();
  @Output() emitSearchedData: EventEmitter<any> = new EventEmitter<any>();
  constructor(private lookup: LookupService, private router: Router, private utilities: UtilitiesService) {


  }

  ngOnInit() {
    this.buttonsList = [
      {
        label: 'Remove', icon: 'fa fa-times', command: () => {
          this.removeContract(this.activeRow.id);
        }
      }
    ];
  }
  setPage(pageInfo) {
    this.page.pageNumber = pageInfo.offset;
    if (this.utilities.paginationFlag == false)
      this.emitAllData.emit(this.page.pageNumber);
    else
      this.emitSearchedData.emit({ pageNumber: this.page.pageNumber, searchedParmaters: 'search' });
  }

  getRowClass(row) {
    if (new Date(row.endDate) < this.todayDate) {
      return 'expired-contract';
    }
    else {
      return 'red';
    }
  }


  ngOnChanges() {
    if (this.contracts.hasOwnProperty("result")) {
      this.contracts = this.contracts.result;
    }

    if (this.contracts.length > 0) {
      if (this.contracts.results != undefined) {
        this.contracts.results.map((contract) => {
          contract.startDate = this.utilities.convertDatetoNormal(contract.startDate);
          contract.endDate = this.utilities.convertDatetoNormal(contract.endDate);
        });
        this.contracts = this.contracts.results;
      }
      else {
        this.contracts.map((contract) => {
          contract.startDate = this.utilities.convertDatetoNormal(contract.startDate);
          contract.endDate = this.utilities.convertDatetoNormal(contract.endDate);
        });
        this.contracts = this.contracts;
      }

    }
  }

  exportCsv() {
 
    let exportData = [];
    exportData.push({
      'Contract Number': 'Contract Number',
      'Type': 'Type',
      'Customer Name': 'Reference no',
      'Start date': 'Start date',
      'End Date': 'End Date',
      'Price': 'Price',
      'Remarks': 'Remarks'
    });
    this.contracts.map((item) => {
      exportData.push({
        'Contract Number': (item.contractNumber == null) ? '' : item.contractNumber,
        'Type': (item.contractType.name == null) ? '' : item.contractType.name,
        'Customer Name': (item.customer.name == null) ? '' : item.customer.name ,
        'Start date': (item.startDate == null) ? '' : item.startDate,
        'End Date': (item.endDate == null) ? '' : item.endDate,
        'Price': (item.price == null) ? '' : item.price,
        'Remarks': (item.remarks == null) ? '' : item.remarks
      })


    });
    return new Angular2Csv(exportData, 'Contracts Report', {
      showLabels: true
    });
  }

  emitPrint(): void {
    this.print.emit();
  }

  routeToEstimation(id) {
    this.router.navigate(['/search/editEstimation/', id]);
  }

  removeContract(ContractId) {
    //console.log(ContractId);
    this.deleteContractSubscription = this.lookup.deleteContract(ContractId).subscribe(() => {
      this.cornerMessage.push({
        severity: 'success',
        summary: 'Successfully!',
        detail: 'Contract removed Successfully!'
      });
      this.contracts = this.contracts.filter((contract) => {
        return contract.id != ContractId;
      });
    },
      err => {
        this.cornerMessage.push({
          severity: 'error',
          summary: 'Failed!',
          detail: 'Failed to remove contract due to server error!'
        })
      })
  }

  setActiveRow(contract) {
    this.activeRow = contract;
    //console.log(this.activeRow);
  }

  routeToGenerateOrder(id) {
    // let contractStringfied = JSON.stringify(contract);
    this.router.navigate(['search/contract/', id])
  }

  /**
   * 
   * @param id 
   */
  navToAddCall(id: number) {
    this.router.navigate(['search/new'], {
      queryParams: {
        customerId: id
      }
    })
  }

}
