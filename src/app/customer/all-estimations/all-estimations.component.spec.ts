import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AllEstimationsComponent } from './all-estimations.component';

describe('AllEstimationsComponent', () => {
  let component: AllEstimationsComponent;
  let fixture: ComponentFixture<AllEstimationsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AllEstimationsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AllEstimationsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
