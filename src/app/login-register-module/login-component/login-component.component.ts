import { AuthenticationServicesService } from "./../../api-module/services/authentication/authentication-services.service";
import { AlertServiceService } from "./../../api-module/services/alertservice/alert-service.service";
import { Component, OnInit } from "@angular/core";
import { Router, ActivatedRoute } from "@angular/router";
import { AuthService } from './../../services/auth.service';

@Component({
  selector: "app-login-component",
  templateUrl: "./login-component.component.html",
  styleUrls: ["./login-component.component.css"],
  providers: [
    AuthService
  ]
})

export class LoginComponentComponent implements OnInit {

  model: any = {};
  loading = false;
  returnUrl: string;

  // private _hubConnection: HubConnection;

  constructor(private route: ActivatedRoute,
    private router: Router,
    private authenticationService: AuthenticationServicesService,
    private alertService: AlertServiceService,
    private authService: AuthService
  ) {
  }

  ngOnInit() {
    if (this.authService.authenticated) {
      this.router.navigate(['/']);
    }

    this.route.queryParamMap.subscribe(res => {
      console.log(res);
    })

    this.returnUrl = this.route.snapshot.queryParams["returnUrl"] || "/";
  }

  login() {

    this.loading = true;
    this.authenticationService.login(this.model.username, this.model.password)
      .subscribe(
        res => {
          this.authService.setUser(res);  
          this.router.navigate([this.returnUrl]);
        },
        error => {
          this.alertService.error("User name or password is incorrect");
          this.loading = false;
        });
  }
}
