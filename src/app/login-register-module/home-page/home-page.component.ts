import { Component, OnInit, ChangeDetectorRef, AfterViewInit, ChangeDetectionStrategy, OnDestroy } from '@angular/core';
import { AuthenticationServicesService } from "./../../api-module/services/authentication/authentication-services.service";

interface Item {
    title: string,
    path: string,
    icon: string
    roles: Array<String>
}

@Component({
    selector: 'app-home-page',
    templateUrl: './home-page.component.html',
    styleUrls: ['./home-page.component.css'],
    changeDetection: ChangeDetectionStrategy.OnPush
})

export class HomePageComponent implements OnInit, AfterViewInit, OnDestroy {

    currentUser: any;
    roles: any[] = [];
    countMenuItems: number;
    menuClass: string = 'col-md-2';

    items: Item[] = [
        {
            title: 'Admin page',
            path: './admin',
            icon: 'fa-cogs',
            roles: [
                'Admin'
            ]
        },
        {
            title: 'Start Call',
            path: './search',
            icon: 'fa-user-plus',
            roles: [
                'CallCenter'
            ]
        },
        {
            title: 'Calls history',
            path: './search/calls-history',
            icon: 'fa-phone',
            roles: [
                'CallCenter', 'Maintenance'
            ]
        },
        {
            title: 'Customers',
            path: './search/customersList',
            icon: 'fa-users',
            roles: [
                'CallCenter', 'Maintenance'
            ]
        },
        {
            title: 'Estimations',
            path: './search/estimation',
            icon: 'fa-file-text',
            roles: [
                'Maintenance'
            ]
        },
        {
            title: 'Quotations',
            path: './search/quotation',
            icon: 'fa-usd',
            roles: [
                'Maintenance', 'CallCenter'
            ]
        },
        {
            title: 'Contracts',
            path: './search/contract',
            icon: 'fa-book',
            roles: [
                'CallCenter'
            ]
        },
        {
            title: 'Orders',
            path: './search/order',
            icon: 'fa-briefcase',
            roles: [
                'CallCenter'
            ]
        },
        {
            title: 'Orders Management',
            path: './dispatcher',
            icon: 'fa-briefcase',
            roles: [
                'SupervisorDispatcher', 'Dispatcher'
            ]
        },
        {
            title: 'Preventive Orders',
            path: './dispatcher/preventive',
            icon: 'fa-history',
            roles: [
                'SupervisorDispatcher', 'Dispatcher'
            ]
        },
        // {
        //   title: 'Estimations Report',
        //   path: './search/estimations-report',
        //   icon:'fa-briefcase',
        //   roles: [
        //   'Maintenance','CallCenter'
        //   ]
        // },
        {
            title: 'Report',
            path: './dispatcher/report',
            icon: 'fa-table',
            roles: [
                'SupervisorDispatcher', 'Dispatcher', 'Admin'
            ]
        },
        {
            title: 'Visit Time Calender',
            path: './dispatcher/calender',
            icon: 'fa-calendar',
            roles: [
                'SupervisorDispatcher', 'Dispatcher'
            ]
        },
        {
            title: 'Inventory',
            path: './assign-item',
            icon: 'fa-industry',
            roles: [
                'SupervisorDispatcher', 'Dispatcher', 'Admin'
            ]
        },
    ];

    constructor(private authService: AuthenticationServicesService,
        private cdr: ChangeDetectorRef,
    ) {
        this.currentUser = JSON.parse(localStorage.getItem('currentUser'));

        setTimeout(() => {
            if (!this.cdr['destroyed']) {
                this.cdr.detectChanges();
            }
        }, 0);

    }

    ngOnInit() {

        // this.loadAllUsers();
        this.authService.userRoles.subscribe((roles) => {
            this.roles = roles || [];
            this.compare(this.roles, this.items)
        });

    }

    ngAfterViewInit() {

    }

    /**
     * 
     * @param source 
     * @param target 
     */
    containsAny(target) {
        let result = this.roles.filter((item) => {
            return target.indexOf(item) > -1;
        });
        return (result.length > 0);
    }



    compare(arr1, arr2) {
        var result = {}

        arr1.forEach((item) => {
            result[item] = 0
        })

        arr2.forEach((arr) => {
            arr.roles.forEach(element => {
                if (result.hasOwnProperty(element)) {
                    result[element]++
                }
            });
        });

        for (const key in result) {
            if (result.hasOwnProperty(key)) {
                const element = result[key];
                this.countMenuItems = element;
            }
        }

        if (this.countMenuItems > 0 && this.countMenuItems <= 3) {
            this.menuClass = 'col-md-4';
        } else {
            this.menuClass = 'col-md-3';
        }
    }


    ngOnDestroy() {
        this.cdr.detach();
    }

}