import { LookupService } from './services/lookup-services/lookup.service';
import { AuthGuardGuard } from './guards/auth-guard.guard';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpModule, BaseRequestOptions } from '@angular/http';

@NgModule({
  imports: [
    CommonModule,
    HttpModule
  ],
  providers: [
    BaseRequestOptions,
    AuthGuardGuard,
    LookupService],

  declarations: []
})
export class ApiModuleModule { }
