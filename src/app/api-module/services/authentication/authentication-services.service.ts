import {Injectable} from '@angular/core';
import {Http, Headers, Response, RequestOptions, RequestMethod} from '@angular/http';
import 'rxjs/add/operator/map'
import * as myGlobals from '../globalPath';
import {Subject} from "rxjs";
import {callHubUrl, quotationHubUrl, orderHubUrl} from "../globalPath";
import {BehaviorSubject} from "rxjs/BehaviorSubject";

let headers = new Headers({'Content-Type': 'application/json'});
headers.append('Access-Control-Allow-Origin', '*');

let options = new RequestOptions({headers: headers});

export enum types {
  order = 1,
  call = 2
}

// let options = new RequestOptions( {method: RequestMethod.Post, headers: headers });
@Injectable()

export class AuthenticationServicesService {
  isLoggedin = new BehaviorSubject(<boolean>false);
  userRoles = new Subject<string[]>();
  quotaionSignalR = new Subject<any>();
  callSignalR = new Subject<any>();
  orderSignalR = new Subject<any>();

  constructor(private http: Http) {
  }


  setLoggedIn(value) {
    // //console.log(value)
    this.isLoggedin.next(value);
  }

  setRoles(value) {
    // //console.log(value)
    this.userRoles.next(value);
  }

  // isLoggedin: boolean = false;

  login(username: string, password: string) {
    return this.http.post(myGlobals.BaseUrlUserManagement + 'token', JSON.stringify({
      userName: username,
      password: password
    }), options)
      .map((response: Response) => {
        // login successful if there's a jwt token in the response  
        let user = response.json();
        // //console.log(user);
        if (user && user.token.accessToken) {
          //console.log('---------- user login -------------- ');
          //console.log(user);
          // store user details and jwt token in local storage to keep user logged in between page refreshes
          localStorage.setItem('currentUser', JSON.stringify(user));
          this.setLoggedIn(true);
          this.setRoles(user.roles)
        }

        return user;
      });
  }

  logout() {
    // remove user from local storage to log user out
    if (this.CurrentUser()) {
      this.hubStop();
    }
    localStorage.removeItem('currentUser');
    this.setLoggedIn(false);
  }

  isLoggedIn() {
    if (localStorage.getItem("currentUser") == null) {
      this.setLoggedIn(false);
      return false;
    }
    else {
      return true;
    }
  }

  CurrentUser() {
    if (localStorage.getItem("currentUser") == null) {
      this.setLoggedIn(false);
      return null;
    }
    else {
      this.setLoggedIn(true);
      this.setRoles(JSON.parse(localStorage.getItem("currentUser")).roles);
      return JSON.parse(localStorage.getItem('currentUser'));
    }
  }

  activateSignalR() {

  }

  hubStart() {

  }

  hubStop() {
  }

  setQuotaionSignalR(quotation) {
    this.quotaionSignalR.next(quotation)
  }

  setCallSignalR(call) {
    this.callSignalR.next(call)
  }

  setOrderSignalR(order) {
    console.log('order signal R updated');
    this.orderSignalR.next(order)
  }

}
