export interface CustomerHistory {
    customer?:customer;
    complains?:Array<complain>;
    contracts?:Array<contract>;
    workOrders?:Array<WorkOrderDetails>;
    isDeleted?: string;
    createdDate?:string;
    updatedDate?:string;
    deletedDate?:string
}
export interface customer {
    id?: string;
    name?: string;
    civilId?: string;
    mobile1?: string;
    mobile2?: string;
    phone?: string;
    remarks?: string;
    companyName?: string;
    division?: string;
    fK_CustomerType_Id?:string;
    fK_Location_Id?:string;
}
export interface complain {
    id?:string;
    note?:string;
    fK_Customer_Id?: string;
}
export interface contract {
    id?:string;
    contractNumber?:string;
    price?:string;
    startDate?:string;
    endDate?:string;
    amount?:string;
    fK_ContractType_Id?:string;
    pendingAmount?: string;
    remarks?: string;
    fK_Customer_Id?: string;
    contractTypeName?:string;
}

export interface WorkOrderDetails{
    workOrder?:workOrderData;
    subOrdersFactoryPercent?:Array<subOrdersFactoryPercent>;
}
    


export interface workOrderData {
    id?: string;
    code?: string;
    pendingAmount?: string;
    amount?:string;
    startDate?:string;
    endDate?:string;
    fK_Location_Id?:string;
    fK_Priority_Id?:string;
    fK_Customer_Id?:string;
    fK_Contract_Id?:string;
    fk_Status_Id?:string;
    items?:Array<Item>;
    hasSuborders?:boolean;
}

export interface Item{
    id?:string;
}

export interface subOrdersFactoryPercent{
    workOrder?:workOrderData;
    factory?:Factory;
    c?: string,
    amount?: string,
    isDeleted?: boolean,
    percentage?:number;
}

export interface Factory{
    id?: string;
    name?: string;
}


    




