import {
  Router,
  CanActivate,
  ActivatedRouteSnapshot,
  RouterStateSnapshot
} from '@angular/router';
import { Injectable } from '@angular/core';
import { AuthService } from './../../services/auth.service';

@Injectable()
export class AuthGuardGuard implements CanActivate {
  public roles: any;

  constructor(
    private router: Router,
    private authService: AuthService
  ) {
  }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    if (this.authService.authenticated) {
      return true;
    } else {
      this.router.navigate(['login'], { queryParams: { returnUrl: state.url } });
      return false;
    }



    // this.roles = route.data["roles"] as Array<string>;

    // if (localStorage.getItem('currentUser')) {
    //   let UserAllRoles = JSON.parse(localStorage.getItem('currentUser')).roles;
    //   if (this.checkRoles(this.roles)) {
    //     return true;
    //   }
    //   else {
    //     if (UserAllRoles.indexOf("Admin") > -1) {
    //       this.router.navigate(['/admin']);
    //       return false;
    //     }
    //     else if (state.url == '/' && (UserAllRoles.indexOf("Dispatcher") > -1 || UserAllRoles.indexOf("MovementController") > -1 ||UserAllRoles.indexOf("SupervisorDispatcher") > -1)) {
    //       this.router.navigate(['/dispatcher']);
    //       return false;
    //     }
    //     else {
    //       this.alertService.error("You don't have permission to Access");
    //       return false;

    //     }

    //   }
    // }

    // // not logged in so redirect to login page with the return url
    // return false;
  }



  // checkRoles(listRoles: Array<string>) {

  //   let UserAllRoles = JSON.parse(localStorage.getItem('currentUser')).roles;
  //   for (var i = 0; i < this.roles.length; i++) {
  //     for (var j = 0; j < UserAllRoles.length; j++) {
  //       if (this.roles[i].toLowerCase() == UserAllRoles[j].toLowerCase()) {
  //         return true;
  //       }

  //     }
  //   }

  //   return false;
  // }
}
