import { HashLocationStrategy, LocationStrategy } from "@angular/common";
import { HttpClient, HttpClientModule } from "@angular/common/http";
import { NgModule } from "@angular/core";
import { FormsModule } from "@angular/forms";
import { BaseRequestOptions, HttpModule } from "@angular/http";
import { BrowserModule } from "@angular/platform-browser";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { MessageService } from "primeng/components/common/messageservice";
import { AuthenticationServicesService } from "./api-module/services/authentication/authentication-services.service";
import { UtilitiesService } from "./api-module/services/utilities/utilities.service";
import { AppComponent } from "./app.component";
import { RoutingComponents, RoutingModule } from "./router.module";
import { SharedModuleModule } from "./shared-module/shared-module.module";
import { DataService } from "./admin/settings/data.service";
import { AuthService } from './services/auth.service';
import { HeaderModule } from './components/header/header.module';
import { FooterModule } from './components/footer/footer.module';
import { AlertModule } from './components/alert/alert.module';

@NgModule({
  declarations: [
    AppComponent,
    RoutingComponents
  ],

  imports: [
    BrowserModule,
    HttpClientModule,
    BrowserAnimationsModule,
    RoutingModule,
    FormsModule,
    HttpModule,
    HeaderModule,
    FooterModule,
    AlertModule,
    HttpClientModule,
    SharedModuleModule.forRoot(),
  ],

  providers: [
    {
      provide: LocationStrategy,
      useClass: HashLocationStrategy
    },
    BaseRequestOptions,
    UtilitiesService,
    MessageService,
    AuthenticationServicesService,
    HttpClient,
    DataService,
    AuthService
  ],

  bootstrap: [AppComponent]
})
export class AppModule {
}
