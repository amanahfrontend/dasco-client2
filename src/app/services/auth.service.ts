import { Injectable } from '@angular/core';

@Injectable()
export class AuthService {

  constructor() { }

  /**
 * set user 
 * 
 * @param user 
 */
  setUser(user: any) {
    return localStorage.setItem('currentUser', JSON.stringify(user));
  }

  /**
   * get user data
   * 
   * 
   */
  get user() {
    if (localStorage.getItem('currentUser') !== null) {
      return JSON.parse(localStorage.getItem('currentUser'));
    }
  }

  /**
   * logout
   * 
   * 
   */
  logout() {
    if (localStorage.getItem('currentUser')) {
      return localStorage.removeItem('currentUser');
    }
  }

  /**
   * check authenticated user
   * 
   * 
   */
  get authenticated(): boolean {
    if (localStorage.getItem('currentUser') !== null) {
      return true;
    }

    return false;
  }

}
